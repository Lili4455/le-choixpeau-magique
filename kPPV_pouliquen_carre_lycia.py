import csv
import math

#création de la table characteristics_tab à partir du fichier Caracteristiques_des_persos.csv
with open("Caracteristiques_des_persos.csv", mode='r', encoding='utf-8') as f:
    reader = csv.DictReader(f, delimiter=';')
    characteristics_tab = [{key: value for key, value in element.items()} for element in reader]

#création de la table characters_tab à partir du fichier Characters.csv
with open("Characters.csv", mode='r', encoding='utf-8') as f:
    reader = csv.DictReader(f, delimiter=';')
    characters_tab = [{key: value for key, value in element.items()} for element in reader]

#jointure des tables characters_tab et characteristics_tab
poudlard_characters = []
for poudlard_character in characteristics_tab:
    for kaggle_character in characters_tab:
        if poudlard_character['Name'] == kaggle_character['Name']:
            poudlard_character.update(kaggle_character)
            poudlard_characters.append(poudlard_character)

#indexation de toute la table par identifiant
index_id = {int(character['Id']): character for character in poudlard_characters}

#indexation des caractéristiques des persos par indentifiant
index_id_characteristics = {int(character['Id']):
                            (int(character['Courage']),
                             int(character['Ambition']),
                             int(character['Intelligence']),
                             int(character['Good']))
                            for character in poudlard_characters}

carateristiques_eleves = index_id_characteristics
k = 5
eleve_exemple = [(9, 2, 8, 9), (6, 7, 9, 7), (3, 8, 6, 3), (2, 3, 7, 8), (3, 4, 8, 8)]


def distance(x, y):
    '''
    Entrée : liste avec les nouveaux élèves, dictionnaire avec les caractéristiques des élèves
    Sortie : nombre décimal

    Cette fonction revoie à partir des caractéristiques d'un élève de Poudelard et des
    caractéristiques d'un nouvel élève, la distance entre ces deux élèves.
    '''
    dist = 0
    for i in range(4):
        dist = dist + (x[i] - y[i])**2
    return math.sqrt(dist)


def kPPV(L, k, x):
    '''
    Entrée : liste avec les nouveaux élèves, dictionnaire avec les caractéristiques des élèves,
    nombre entier
    Sortie : liste avec les identifiants des k plus proches voisins

    Cette fonction revoie à partir des caractéristiques d'un élève de Poudelard, des
    caractéristiques d'un nouvel élève et du nombre de voisins que l'ont prend, une
    liste des identifiants des k plus proches voisins.
    '''
    liste_distance_indice = []
    for element in index_id_characteristics:
        d = distance(x, L[element])
        liste_distance_indice.append([d, element])
    liste_distance_indice.sort()
    Voisins = []
    for i in range(k):
        Voisins.append(liste_distance_indice[i][1])
    return Voisins


def choixpeau(L, k, x):
    '''
    Entrée : liste avec les nouveaux élèves, dictionnaire avec les caractéristiques des élèves,
    nombre entier
    Sortie : chaine de caractères avec la maison du nouvel élève

    Cette fonction revoie à partir des caractéristiques d'un élève de Poudelard, des
    caractéristiques d'un nouvel élève et du nombre de voisins que l'ont prend, la
    maison du nouvel élève.
    '''
    id_voisins = kPPV(L, k, x)
    nom_voisins = []
    maisons_voisins = []
    for i in range(0, k):
        nom_voisins.append(index_id[id_voisins[i]]['Name'])
        maisons_voisins.append(index_id[id_voisins[i]]['House'])
    print(nom_voisins)
    print(maisons_voisins)
    maisons_possibles = ['Gryffindor', 'Ravenclaw', 'Slytherin', 'Hufflepuff']
    decompte = [0, 0, 0, 0]
    for i in range(k):
        if maisons_voisins[i] == 'Gryffindor':
            decompte[0] += 1
        if maisons_voisins[i] == 'Ravenclaw':
            decompte[1] += 1
        if maisons_voisins[i] == 'Slytherin':
            decompte[2] += 1
        if maisons_voisins[i] == 'Hufflepuff':
            decompte[3] += 1
    winner = decompte[0]
    indice = 0
    if decompte[1] > winner:
        indice = 1
        winner = decompte[1]
    if decompte[2] > winner:
        indice = 2
        winner = decompte[2]
    if decompte[3] > winner:
        indice = 3
    return maisons_possibles[indice]

#IHM avec le minimum
demande = int(input("Essais automatiques (1), saisie manuelle(2), sortir(3) : "))
while demande != 3:
    if demande == 1:
        print("Maison du 1er nouvel élève : ", choixpeau(carateristiques_eleves, k, eleve_exemple[0]))
        print("Maison du 2eme nouvel élève : ", choixpeau(carateristiques_eleves, k, eleve_exemple[1]))
        print("Maison du 3eme nouvel élève : ", choixpeau(carateristiques_eleves, k, eleve_exemple[2]))
        print("Maison du 4eme nouvel élève : ", choixpeau(carateristiques_eleves, k, eleve_exemple[3]))
        print("Maison du 5eme nouvel élève : ", choixpeau(carateristiques_eleves, k, eleve_exemple[4]))
    if demande == 2:
        nouvel_eleve = []
        courage = int(input('Saisissez le profil du nouvel élève, son courage : '))
        nouvel_eleve. append(courage)
        ambition = int(input("son ambition : "))
        nouvel_eleve. append(courage)
        intelligence = int(input("son intelligence : "))
        nouvel_eleve. append(intelligence)
        good = int(input("sa gentillesse : "))
        nouvel_eleve.append(good)
        print(nouvel_eleve)
        print("Maison du 5eme nouvel élève : ", choixpeau(carateristiques_eleves, k, nouvel_eleve))
    demande = int(input("Essais automatiques (1), saisie manuelle(2), sortir(3)"))
